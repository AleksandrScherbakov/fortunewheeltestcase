/**
 * Created by Aleksandr Scherbakov on 26.03.2017.
 */
define(["scripts/config.js",
        "scripts/libs/pixi.min.js"],
function(cfg, PIXI)
{
    return function ResetButton(positionX, positionY)
    {
        this._resetButtonSprite = new PIXI.Sprite(PIXI.TextureCache[cfg.resetButtonImage]);
        stage.addChild(this._resetButtonSprite);
        this._resetButtonSprite.anchor.set(cfg.SPRITE_CENTER, cfg.SPRITE_CENTER);
        this._resetButtonSprite.position.set(positionX, positionY);
        this._resetButtonSprite.interactive = true;

        this._resetButtonSprite.click = function (mouseData)
        {
            if (!fortuneWheel._spinning)
            {
                fortuneWheel._wheelSprite.rotation = 0;
                fortuneWheel.setCurrentAngle(0);
            }
        };
    }
});