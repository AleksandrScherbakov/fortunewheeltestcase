/**
 * Created by Aleksandr Scherbakov on 26.03.2017.
 */
define(["scripts/config.js",
        "scripts/libs/pixi.min.js"],
function(cfg, PIXI)
{
    return function SpinButton(positionX, positionY)
    {
        this._spinButtonSprite = new PIXI.Sprite(PIXI.TextureCache[cfg.spinButtonImage]);
        stage.addChild(this._spinButtonSprite);
        this._spinButtonSprite.anchor.set(cfg.SPRITE_CENTER, cfg.SPRITE_CENTER);
        this._spinButtonSprite.position.set(positionX, positionY);
        this._spinButtonSprite.interactive = true;

        this._spinButtonSprite.click = function (mouseData)
        {
            console.log("sdfsdf");
            var sel = document.getElementById("selectSector");
            var val = sel.options[sel.selectedIndex].value;
            if (!fortuneWheel._spinning)
            {
                fortuneWheel._spinning = !fortuneWheel._spinning;
                fortuneWheel.setWheel(20, 8, val);
                fortuneWheel.setSpinTime(0);
            }
        };
    }
});