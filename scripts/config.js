/**
 * Created by Aleksandr Scherbakov on 26.03.2017.
 */

define(function()
{
    var cfg =
        {
        screenWidth : 1366,
        screenHeight : 768,
        MILLISEC_PER_SEC : 1000,
        SECTOR_COUNT : 12,
        FULL_ANGLE : 360,
        SPRITE_CENTER : 0.5,
        AZURE_BLUE : 0x1099bb,

        fortuneWheelImage : "wheel_of_fortune.png",
        spinButtonImage : "spin_button.png",
        wheelArrowImage : "arrow.png",
        resetButtonImage : "reset_button.png",
        texturePackPath : "assets/sprites/texPack.json"
        }

        return cfg;
});
