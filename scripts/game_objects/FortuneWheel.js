/**
 * Created by Aleksandr Scherbakov on 25.03.2017.
 */
define(["scripts/config.js",
        "scripts/libs/pixi.min.js",
        "scripts/game_objects/WheelArrow.js",
        "scripts/libs/cubic-spline.js"],
function(cfg, PIXI, WheelArrow)
{
    return function FortuneWheel(sectorCount, positionX, positionY)
    {
        //private properties
        var circlesToSpin = 10,
            sectorToSpin = 0,
            currentAngle = 0,
            sectorPart = 0,
            angleToSpin = 0,
            time = 0,
            timeToSpin = 10;

        //public properties
        this._sectorCount = sectorCount;
        this._spinning = false;
        this._degreeOnSector = cfg.FULL_ANGLE / sectorCount;
        this._wheelSprite = new PIXI.Sprite(PIXI.TextureCache[cfg.fortuneWheelImage]);


        //constructor initializing
        this._wheelSprite.anchor.set(cfg.SPRITE_CENTER, cfg.SPRITE_CENTER);
        this._wheelSprite.position.set(positionX, positionY);
        stage.addChild(this._wheelSprite);
        this._arrow = new WheelArrow(positionX, positionY - 320);

        //Setters
        this.setSpinTime = function (settedTime)
        {
            time = settedTime;
        };

        this.setWheel = function (circles, spinTime, sector)
        {
            circlesToSpin = circles;
            timeToSpin = spinTime;
            sectorToSpin = (this._sectorCount - sector) % this._sectorCount + 1;

            var fluctuationRate = 0.25;
            sectorPart = fluctuationRate * (Math.random() - 0.5);

            var angleToSpinDegree = circlesToSpin * cfg.FULL_ANGLE + this._degreeOnSector * (sectorToSpin + sectorPart);
            angleToSpin = Math.PI * angleToSpinDegree / 180 - currentAngle % (2 * Math.PI);
        };

        this.setCurrentAngle = function (angle)
        {
            currentAngle = angle;
        };

        //public methods
        this.spin = function ()
        {
            if (this._spinning)
            {
                if (time < timeToSpin)
                {
                    this._wheelSprite.rotation = currentAngle + angleToSpin * spinPolicy(time / timeToSpin, 0.5, Math.PI / 12);
                    time += delta;
                }
                else
                {
                    currentAngle = this._wheelSprite.rotation;
                    this._spinning = false;
                }
            }
        };

        this.reset = function ()
        {
            this._wheelSprite.rotation = 0;
        };

        var spinPolicy = function (arg, rollbackTime, rollbackAngle)
        {
            var backPartTime = rollbackTime / timeToSpin;
            var backPartAngle = - rollbackAngle / angleToSpin;

            //spline to smooth speed changing
            var xs = [0, backPartTime, 2 * backPartTime, 0.55, 1 - 0.5 / timeToSpin, 1];
            var ys = [0, backPartAngle, 0, 0.72, 1, 1];

            return spline(arg, xs, ys);
        };
    }
});

