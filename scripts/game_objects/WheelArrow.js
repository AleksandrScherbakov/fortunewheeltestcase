/**
 * Created by Aleksandr Scherbakov on 26.03.2017.
 */

define(["scripts/config.js",
        "scripts/libs/pixi.min.js"],
function(cgf, PIXI)
{
    return function WheelArrow(positionX, positionY)
    {
        this._arrowSprite = new PIXI.Sprite(PIXI.TextureCache[cgf.wheelArrowImage]);
        stage.addChild(this._arrowSprite);
        this._arrowSprite.anchor.set(cgf.SPRITE_CENTER, 0.2);
        this._arrowSprite.position.set(positionX, positionY);
    }
});