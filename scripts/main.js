/**
 * Created by Aleksandr Scherbakov on 27.03.2017.
 */

require(["scripts/libs/pixi.min.js",
         "scripts/config.js",
         "scripts/game_objects/FortuneWheel.js",
         "scripts/buttons/SpinButton.js",
         "scripts/buttons/ResetButton.js"],
function(PIXI, cfg, FortuneWheel, SpinButton, ResetButton)
{
    stage = new PIXI.Container();
    renderer = new PIXI.WebGLRenderer(cfg.screenWidth, cfg.screenHeight, {backgroundColor: cfg.AZURE_BLUE});
    document.body.appendChild(renderer.view);
    PIXI.loader
        .add(cfg.texturePackPath)
        .load(setup);

    function setup()
    {
        fortuneWheel = new FortuneWheel(SECTOR_COUNT, cfg.screenWidth * 0.5, cfg.screenHeight * 0.5);
        spinButton = new SpinButton(cfg.screenWidth * 0.8, cfg.screenHeight * 0.8);
        resetButton = new ResetButton(cfg.screenWidth * 0.2, cfg.screenHeight * 0.9);

        state = play;
        gameLoop();
    }

    function gameLoop()
    {
        requestAnimationFrame(gameLoop);
        currentTime = (new Date()).getTime();
        delta = (currentTime - lastTime) / cfg.MILLISEC_PER_SEC;
        state();
        lastTime = currentTime;
        renderer.render(stage);
    }

    function play()
    {
        fortuneWheel.spin();
    }

    function end()
    {
        //not defined yet
    }
}
);